import {Options} from 'ky';

type UseFetchProps = {
  path: string;
  options: Options;
}

type UseFetchReturn<T> = {
  loading: boolean;
  error?: Error;
  response?: T;
}

export type {
  UseFetchProps,
  UseFetchReturn,
};
