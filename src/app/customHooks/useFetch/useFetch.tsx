import {useEffect, useState} from 'react';

import HttpService from 'app/services/HttpService';

import {UseFetchProps, UseFetchReturn} from './useFetch.types';


const useFetch = <T extends {}>({path, options = {}}: UseFetchProps): UseFetchReturn<T> => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error>();
  const [response, setResponse] = useState<T>();

  const logAndSetError = (error: Error) => {
    console.error(error);
    setError(error);
  };

  useEffect(() => {
    setLoading(true);
    HttpService.request(path, options)
    .then((res) => setResponse(res as T))
    .catch((error: Error) => logAndSetError(error))
    .finally(() => setLoading(false));
  }, [path, options]);

  return {
    loading,
    error,
    response,
  };
};

export default useFetch;
