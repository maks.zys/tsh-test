import useFetch from 'app/customHooks/useFetch/useFetch';

export {
  useFetch,
};
