import {Options} from 'ky';

type RepositoryReturn = {
  path: string;
  options: Options;
}

export type {
  RepositoryReturn,
};
