type ProductResponse_GET = {
  'items': {
    id: number;
    name: string;
    description: string;
    rating: number;
    image: string;
    promo: boolean;
    active: boolean;
  }[],
  'itemCount': number;
  'totalItems': number;
  'pageCount': number;
  'next': string;
  'previous': string;
}

export type {
  ProductResponse_GET,
};
