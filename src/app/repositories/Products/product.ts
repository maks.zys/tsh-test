import RepositoryService from 'app/services/RepositoryService';

import {RepositoryReturn} from 'app/repositories/repositories.types';

type ProductsSearchParams = {
  page: number;
  limit: number;
  search?: string;
  promo?: boolean;
  active?: boolean;
}

/**
 * [Swagger]{@link https://join-tsh-api-staging.herokuapp.com/docs/#/default/ProductController_findAll}
 */
function getProducts(params: ProductsSearchParams): RepositoryReturn {
  return {
    path: 'product',
    options: {
      searchParams: RepositoryService.objectIntoURLSearchParams(params),
    },
  };
}

export {
  getProducts,
};
