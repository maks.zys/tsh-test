import {getProducts} from './product';

// types
import {ProductResponse_GET} from './product.types';

export {
  getProducts,
};

export type {
  ProductResponse_GET,
};
