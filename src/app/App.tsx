import React from 'react';
import 'normalize.css';

import { AppRoutes } from 'routing/AppRoutes';

export const App = () => {
  return <AppRoutes />;
};
