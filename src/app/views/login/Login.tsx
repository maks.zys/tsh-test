import React from 'react';
import {useHistory} from 'react-router-dom';

import {AppRoute} from 'routing/AppRoute.enum';
import {Button, Icon, Input, Link as UrlLink} from 'app/UI/components';

import styles from 'app/views/login/Login.module.scss';


export const Login = () => {
  const history = useHistory();

  const redirect = () => history.push(AppRoute.home);

  return (
    <div className={styles.Container}>
      <div className={styles.LeftSide}/>
      <div className={styles.RightSide}>
        <div className={styles.RightSideContainer}>
          <Icon src='logo' alt='Join TSH logo' className={styles.Logo}/>
          <div className={styles.FormContainer}>
            <h2 className={styles.Title}>Login</h2>
            <form>
              <Input name='username' label='Username' placeholder='Enter username'/>
              <Input name='password' label='Password' type='password' placeholder='Enter password'/>
              <Button type="submit" isPrimary className={styles.LogInButton} onClick={redirect}>Log in</Button>
            </form>
            <UrlLink url='/' label='Forgot password?'/>
          </div>
        </div>
      </div>
    </div>
  );
};
