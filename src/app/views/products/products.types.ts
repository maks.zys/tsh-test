import {ReducerTypes} from './consts';

type ReducerAction = {
  type: ReducerTypes;
  payload?: any;
}

type ReducerState = {
  page: number;
  active: boolean;
  promo: boolean;
  search: string;
}

export type {
  ReducerState,
  ReducerAction,
};
