import React, {useMemo, useReducer} from 'react';

import {useFetch} from 'app/customHooks';
import {RepositoryReturn} from 'app/repositories/repositories.types';
import {getProducts, ProductResponse_GET} from 'app/repositories/Products';
import {EmptyCard, Error, Header, Pagination, ProductCard, Spinner} from 'app/UI/components';

import {ReducerTypes} from 'app/views/products/consts';
import {ReducerAction, ReducerState} from 'app/views/products/products.types';
import styles from './Products.module.scss';


const initialState: ReducerState = {
  page: 1,
  active: false,
  promo: false,
  search: '',
};

function reducer(state: ReducerState = initialState, action: ReducerAction): ReducerState {
  switch (action.type) {
    case ReducerTypes.page: {
      return {
        ...state,
        page: action.payload,
      };
    }
    case ReducerTypes.active: {
      return {
        ...state,
        active: action.payload || !state.active,
      };
    }
    case ReducerTypes.promo: {
      return {
        ...state,
        promo: action.payload || !state.promo,
      };
    }
    case ReducerTypes.search: {
      return {
        ...state,
        search: action.payload,
      };
    }
  }

  return state;
}


export const Products = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const memorizedRequestData: RepositoryReturn = useMemo(() => {
    return getProducts({...state, limit: 8});
  }, [state]);

  const {loading, error, response} = useFetch<ProductResponse_GET>(memorizedRequestData);

  const setValueAndSetPage = (type: ReducerTypes, payload: any, pageNumber: number = 1) => {
    dispatch({type, payload});
    dispatch({type: ReducerTypes.page, payload: pageNumber});
  };

  const renderDetails = () => {
    if (loading) {
      return <Spinner/>;
    }

    if (error) {
      return <Error/>;
    }

    if (response?.items && response?.items.length === 0) {
      return <EmptyCard/>;
    }

    return (
      <>
        {response!.items.map((responseItem) => (
          <ProductCard
            key={responseItem.id}
            {...responseItem}
            cardClassName={styles.Card}/>
        ))}
        <Pagination
          pageCount={response?.pageCount || 1}
          activePage={state.page}
          onClick={(index) => dispatch({type: ReducerTypes.page, payload: index})}
        />
      </>
    );
  };

  return (
    <>
      <Header
        searchValue={state.search}
        setPromo={(isPromo) => setValueAndSetPage(ReducerTypes.promo, isPromo)}
        setActive={(isActive) => setValueAndSetPage(ReducerTypes.active, isActive)}
        setSearch={(searchValue) => setValueAndSetPage(ReducerTypes.search, searchValue)}
      />
      <div className={styles.CardsContainer}>
        {renderDetails()}
      </div>
    </>
  );
};
