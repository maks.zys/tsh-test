enum ReducerTypes {
  page,
  active,
  promo,
  search,
}

export {
  ReducerTypes,
};
