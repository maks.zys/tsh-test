import React, {FC} from 'react';
import classNames from 'classnames';

import {ButtonProps} from './Button.types';
import styles from './Button.module.scss';


const Button: FC<ButtonProps> = ({type = 'button', ...props}) => (
  <button
    onClick={props.onClick}
    className={classNames(
      props.className,
      {[styles.Primary]: props.isPrimary},
      {[styles.Default]: !props.isPrimary},
    )}
    disabled={props.isDisabled}
    type={type}
  >
    {props.children}
  </button>
);

export default Button;
