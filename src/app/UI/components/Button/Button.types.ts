import {MouseEvent} from 'react';


type ButtonTypes = 'submit' | 'reset' | 'button';

type ButtonProps = Partial<{
  onClick: (event: MouseEvent) => void;
  isPrimary: boolean;
  isDisabled: boolean;
  className: string;
  type: ButtonTypes;
}>;

export type {
  ButtonProps,
};
