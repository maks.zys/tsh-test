import {MouseEvent} from 'react';
import {AVAILABLE_ICONS} from './consts';

type IconsList = keyof typeof AVAILABLE_ICONS;

type IconProps = {
  onClick?: (event: MouseEvent) => void,
  className?: string,
  src: IconsList,
  alt: string,
  svg?: boolean,
}

export type {
  IconProps,
};
