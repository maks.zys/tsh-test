import search from 'assets/icons/search.svg';
import check from 'assets/icons/check.svg';
import logo from 'assets/logo-tsh.svg';
import close from 'assets/icons/close.svg';
import profile from 'assets/icons/Rectangle.png';
import notFound from 'assets/icons/Group.svg';

const AVAILABLE_ICONS = {
  search,
  check,
  logo,
  close,
  profile,
  notFound,
};

export {
  AVAILABLE_ICONS,
};
