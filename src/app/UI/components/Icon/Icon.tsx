import React, {FC} from 'react';
import classNames from 'classnames';

import {AVAILABLE_ICONS} from './consts';
import {IconProps} from './Icon.types';
import styles from './Icon.module.scss';


const Icon: FC<IconProps> = props => (
  <div onClick={props.onClick} className={classNames(styles.Container, props.className)}>
    <img src={AVAILABLE_ICONS[props.src]} alt={props.alt}/>
  </div>
);

export default Icon;
