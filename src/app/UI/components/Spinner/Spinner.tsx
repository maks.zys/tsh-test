import React from 'react';

import styles from './Spinner.module.scss';


const Spinner = () => (
  <div className={styles.Container}>
    <div className={styles.Spinner}/>
  </div>
);

export default Spinner;
