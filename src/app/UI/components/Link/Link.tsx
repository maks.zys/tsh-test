import React, {FC} from 'react';

import {LinkProps} from './Link.types';
import styles from './Link.module.scss';


const Link: FC<LinkProps> = props => (
  <a className={styles.Link} href={props.url}>{props.label}</a>
);

export default Link;
