type LinkProps = {
  url: string;
  label: string;
}

export type {
  LinkProps,
};
