import {MouseEvent} from 'react';
import {IconProps} from 'app/UI/components/Icon/Icon.types';


type IconWithSelectProps = {
  className?: string;
  icon: IconProps;
  selectItems: {
    text: string;
    onClick: (event: MouseEvent) => void;
  }[];
}

export type {
  IconWithSelectProps,
};
