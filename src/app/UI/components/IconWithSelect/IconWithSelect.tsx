import React, {useState} from 'react';
import classNames from 'classnames';

import {Icon} from 'app/UI/components/index';

import {IconWithSelectProps} from './IconWithSelect.types';
import styles from './IconWithSelect.module.scss';


const IconWithSelect = ({icon, selectItems, className}: IconWithSelectProps) => {
  const [showDetails, setShowDetails] = useState(false);

  return (
    <div
      className={classNames(className, styles.Container)}
      onMouseOver={() => setShowDetails(true)}
      onMouseLeave={() => setShowDetails(false)}
      onClick={() => setShowDetails(!showDetails)}
    >
      <Icon {...icon} className={classNames(icon.className, styles.Icon)}/>
      {showDetails &&
      <div className={styles.OptionsContainer}>
        {selectItems.map((item, index) => <div key={index} className={styles.Option} onClick={item.onClick}>{item.text}</div>)}
      </div>
      }
    </div>
  );
};

export default IconWithSelect;
