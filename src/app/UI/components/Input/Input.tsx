import React, {FC, KeyboardEvent} from 'react';

import {InputProps} from './Input.types';
import styles from './Input.module.scss';


const Input: FC<InputProps> = ({type = 'text', acceptedKeys = ['Enter'], ...props}) => {

  const onKeyDownValidation = (event: KeyboardEvent<HTMLInputElement>) => {
    const {key} = event;

    if (props.onKeyDown && acceptedKeys?.includes(key)) {
      props.onKeyDown(event);
    }
  };

  return (
    <div className={styles.Container}>
      {props.label && <span className={styles.Label}>{props.label}</span>}
      <input
        name={props.name}
        className={styles.Input}
        placeholder={props.placeholder}
        onChange={props.onChange}
        onKeyDown={onKeyDownValidation}
        value={props.value}
        type={type}
      />
    </div>
  );
};

export default Input;
