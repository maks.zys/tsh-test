import {ChangeEvent, KeyboardEvent} from 'react';

type InputTypes =
// "button"
// | "checkbox"
  | 'color'
  | 'date'
  | 'datetime-local'
  | 'email'
  | 'file'
  | 'hidden'
  // | "image"
  | 'month'
  | 'number'
  | 'password'
  // | "radio"
  // | "range"
  | 'reset'
  | 'search'
  | 'submit'
  | 'tel'
  | 'text'
  | 'time'
  | 'url'
  | 'week';

type InputProps = {
  name: string;
  label?: string;
  value?: string;
  placeholder?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (event: KeyboardEvent<HTMLInputElement>) => void;
  acceptedKeys?: string[];
  type?: InputTypes;
}

export type {
  InputProps,
};
