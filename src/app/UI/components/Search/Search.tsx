import React, {FC, useEffect, useState} from 'react';
import classNames from 'classnames';

import {Icon, Input} from 'app/UI/components';

import {SearchProps} from './Search.types';
import styles from './Search.module.scss';


const Search: FC<SearchProps> = ({name = 'search', ...props}) => {
  const [text, setText] = useState('');

  useEffect(() => {
    if (props.value) {
      setText(props.value);
    }
  }, [props.value]);

  return (
    <div className={classNames(styles.Container, props.className)}>
      <Input
        name={name}
        value={text}
        placeholder={props.placeholder}
        onChange={(event) => setText(event.target.value)}
        onKeyDown={(() => props.onKeyDown && props.onKeyDown(text))}
      />
      <Icon src='search' alt='Search icon' onClick={() => props.onClick(text)} className={styles.Icon}/>
    </div>
  );
};

export default Search;
