type SearchProps = {
  name?: string;
  value?: string;
  className?: string;
  placeholder?: string;
  onClick: (text: string) => void;
  onKeyDown?: (text: string) => void;
}

export type {
  SearchProps,
};
