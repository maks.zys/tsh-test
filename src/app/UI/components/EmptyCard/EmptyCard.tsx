import React from 'react';

import {Icon} from 'app/UI/components/index';

import styles from './EmptyCard.module.scss';


const EmptyCard = () => (
  <div className={styles.Container}>
    <div>
      <Icon src='notFound' alt='Empty icon'/>
      <h3>Ooops… It’s empty here</h3>
      <span>There are no products on the list</span>
    </div>
  </div>
);

export default EmptyCard;
