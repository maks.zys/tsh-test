import React, {FC, useEffect, useState} from 'react';

import {ReactComponent as Check} from 'assets/icons/check.svg';

import {CheckboxProps} from './Checkbox.types';
import styles from './Checkbox.module.scss';


const Checkbox: FC<CheckboxProps> = props => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(Boolean(props.isActive));
  }, [props.isActive]);

  const onClick = () => {
    setChecked((isChecked) => !isChecked);
    if (props.onClick) {
      props.onClick(checked);
    }
  };


  return (
    <label className={styles.Container}>
      <input type='checkbox' checked={checked} className={styles.Checkbox} onChange={onClick}/>
      <div className={checked ? styles.Checked : styles.Unchecked}>
        {checked && <Check className={styles.CheckIcon}/>}
      </div>
      {props.label && <span className={styles.Label}>{props.label}</span>}
    </label>
  );
};

export default Checkbox;
