type CheckboxProps = {
  onClick?: (event: boolean) => void;
  isActive?: boolean;
  label?: string;
}

export type {
  CheckboxProps,
};
