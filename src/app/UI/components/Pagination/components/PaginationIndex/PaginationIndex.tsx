import React, {FC} from 'react';
import classNames from 'classnames';

import {PaginationIndexProps} from './PaginationIndex.types';
import styles from './PaginationIndex.module.scss';


const PaginationIndex: FC<PaginationIndexProps> = ({
                                                     onClick,
                                                     index,
                                                     label,
                                                     isActive = false,
                                                     isDisabled = false,
                                                     isSideItem = false,
                                                   }) => (
  <button
    onClick={() => onClick(index)}
    className={classNames(
      styles.Button,
      {[styles.ActiveButton]: isActive},
      {[styles.SideItem]: isSideItem},
    )}
    disabled={isDisabled}
  >
    {label || index}
  </button>
);

export default PaginationIndex;
