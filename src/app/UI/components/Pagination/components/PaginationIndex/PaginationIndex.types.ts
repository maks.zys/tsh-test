type PaginationIndexProps = {
  onClick: (index: number) => void;
  index: number;
  label?: string;
  isDisabled?: boolean;
  isActive?: boolean;
  isSideItem?: boolean;
}

export type {
  PaginationIndexProps,
};
