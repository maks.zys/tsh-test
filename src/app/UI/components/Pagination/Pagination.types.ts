import {ReactNode} from 'react';

type PaginationProps = {
  pageCount: number;
  activePage: number;
  onClick: (index: number) => void;
  firstIndex?: number;
}

type PaginationComponents = {
  beforeDots: ReactNode[];
  afterDots: ReactNode[];
}

export type {
  PaginationProps,
  PaginationComponents,
};
