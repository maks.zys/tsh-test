import React, {FC} from 'react';

import {PaginationIndex} from './components/';
import {PaginationComponents, PaginationProps} from './Pagination.types';
import styles from './Pagination.module.scss';


const Pagination: FC<PaginationProps> = ({firstIndex = 1, ...props}) => {
  const DISPLAYED_ELEMENTS = 3;

  if (props.pageCount <= 1) {
    return (
      <div className={styles.Container}>
        <PaginationIndex onClick={props.onClick} index={firstIndex} label='First'/>
      </div>
    );
  }

  const components: PaginationComponents = {
    beforeDots: [],
    afterDots: [],
  };

  if (props.pageCount < 7) {
    for (let i = 1; i <= props.pageCount; i++) {
      components.beforeDots.push(
        <PaginationIndex
          onClick={props.onClick}
          index={i}
          isActive={props.activePage === i}
          key={`before=${i}`}
        />,
      );
    }

    return (
      <div className={styles.Container}>
        <PaginationIndex
          onClick={props.onClick}
          index={firstIndex}
          label='First'
          isSideItem
          isDisabled={props.activePage === firstIndex}
        />
        {components.beforeDots}
        <PaginationIndex
          onClick={props.onClick}
          index={props.pageCount}
          label='Last'
          isSideItem
          isDisabled={props.activePage === props.pageCount}
        />
      </div>
    );
  }

  // Display corners
  if (props.activePage <= DISPLAYED_ELEMENTS - 1 || props.activePage > props.pageCount - DISPLAYED_ELEMENTS - 1) {
    for (let i = firstIndex; i <= DISPLAYED_ELEMENTS; i++) {
      components.beforeDots.push(
        <PaginationIndex
          onClick={props.onClick}
          index={i}
          isActive={props.activePage === i}
          key={`before=${i}`}
        />);
      components.afterDots.unshift(
        <PaginationIndex
          onClick={props.onClick}
          index={props.pageCount - i + 1}
          isActive={props.activePage === (props.pageCount - i + 1)}
          key={`after=${i}`}
        />);
    }

    return (
      <div className={styles.Container}>
        <PaginationIndex
          onClick={props.onClick}
          index={firstIndex}
          label='First'
          isSideItem
          isDisabled={props.activePage === firstIndex}
        />
        {components.beforeDots}
        ...
        {components.afterDots}
        <PaginationIndex
          onClick={props.onClick}
          index={props.pageCount}
          label='Last'
          isSideItem
          isDisabled={props.activePage === props.pageCount}
        />
      </div>
    );
  }

  // Display active as a middle one
  for (let i = firstIndex; i <= DISPLAYED_ELEMENTS - 1; i++) {
    components.beforeDots.unshift(
      <PaginationIndex
        onClick={props.onClick}
        index={props.activePage - i}
        key={`before=${i}`}
        isActive={i === props.activePage}
      />);
    components.afterDots.push(
      <PaginationIndex
        onClick={props.onClick}
        index={props.activePage + i}
        key={`after=${i}`}
        isActive={i === props.activePage}
      />);
  }

  return (
    <div className={styles.Container}>
      <PaginationIndex onClick={props.onClick} index={firstIndex} label='First' isSideItem
                       isDisabled={props.activePage === firstIndex}/>
      ...
      {components.beforeDots}
      <PaginationIndex onClick={props.onClick} index={props.activePage} isActive/>
      {components.afterDots}
      ...
      <PaginationIndex onClick={props.onClick} index={props.pageCount} label='Last' isSideItem
                       isDisabled={props.activePage === props.pageCount}/>
    </div>
  );
};

export default Pagination;
