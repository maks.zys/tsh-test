import React, {useReducer} from 'react';
import {useHistory} from 'react-router-dom';

import {Checkbox, Icon, IconWithSelect, Search} from 'app/UI/components';

import {ReducerTypes} from './consts';
import {HeaderProps, ReducerAction, ReducerState} from './Header.types';
import styles from './Header.module.scss';
import {AppRoute} from 'routing/AppRoute.enum';


function reducer(state: ReducerState, action: ReducerAction) {
  switch (action.type) {
    case ReducerTypes.searchChange: {
      return {
        ...state,
        search: action.payload,
      };
    }
    case ReducerTypes.actionToggle: {
      return {
        ...state,
        active: !state.active,
      };
    }
    case ReducerTypes.promoToggle: {
      return {
        ...state,
        promo: !state.promo,
      };
    }
  }
}

const initialState: ReducerState = {
  search: '',
  active: false,
  promo: false,
};


const Header = (props: HeaderProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const history = useHistory();

  const redirect = () => history.push(AppRoute.login);

  const searchSubmit = (text: string) => {
    props.setSearch(text);
    dispatch({type: ReducerTypes.searchChange, payload: text});
  };

  return (
    <div className={styles.Container}>
      <Icon className={styles.Icon} src='logo' alt='Join tsh logo'/>
      <div className={styles.OptionsContainer}>
        <Search
          placeholder='Search'
          className={styles.SearchEdit}
          value={props.searchValue}
          onClick={searchSubmit}
          onKeyDown={searchSubmit}
        />
        <div className={styles.CheckboxesContainer}>
          <Checkbox
            label='Active'
            onClick={() => {
              props.setActive(!state.active);
              dispatch({type: ReducerTypes.actionToggle});
            }}
          />
          <Checkbox
            label='Promo'
            onClick={() => {
              props.setPromo(!state.promo);
              dispatch({type: ReducerTypes.promoToggle});
            }}
          />
        </div>
      </div>
      <IconWithSelect
        className={styles.IconWithSelect}
        icon={{
          src: 'profile',
          alt: 'profile picture',
          className: styles.ProfilePicture,
        }}
        selectItems={[
          {
            text: 'Log out',
            onClick: redirect,
          },
        ]}
      />
    </div>
  );
};

export default Header;
