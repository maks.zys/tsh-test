import {ReducerTypes} from './consts';

type ReducerAction = {
  type: ReducerTypes;
  payload?: any;
  callback?: (state: ReducerState) => void;
}

type ReducerState = {
  search: string;
  active: boolean;
  promo: boolean;
}

type HeaderProps = {
  searchValue: string;
  setActive: (value: boolean) => void;
  setPromo: (value: boolean) => void;
  setSearch: (value: string) => void;
}

export type {
  ReducerAction,
  ReducerState,
  HeaderProps,
};
