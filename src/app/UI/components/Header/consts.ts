enum ReducerTypes {
  searchChange,
  actionToggle,
  promoToggle,
}

export {
  ReducerTypes,
};
