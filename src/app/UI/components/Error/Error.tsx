import React from 'react';

import styles from './Error.module.scss';


const Error = () => (
  <div className={styles.Container}>
    <img src='https://www.cyberdefinitions.com/images/GIF_example.gif' alt='Screaming woman gif'/>
    <h3>Something gone terribly wrong!!</h3>
    <span>Please refresh the page and I promise this will never happen again! (I hope :-D )</span>
  </div>
);

export default Error;
