type ProductCardBasicProps = {
  id: number;
  name: string;
  description: string;
  rating: number;
  image: string;
  promo: boolean;
  active: boolean;
}

type ProductCardProps = ProductCardBasicProps & {
  cardClassName?: string;
  detailsClassName?: string;
}

export type {
  ProductCardProps,
  ProductCardBasicProps,
};
