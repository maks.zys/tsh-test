import {ProductCardBasicProps} from '../../ProductCard.types';

type CardProps = ProductCardBasicProps & {
  setModalActive: () => void;
  className?: string;
}

export type {
  CardProps,
};
