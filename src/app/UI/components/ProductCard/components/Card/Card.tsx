import React, {FC} from 'react';
import classNames from 'classnames';

import {ReactComponent as Star} from 'assets/icons/star.svg';
import {ReactComponent as EmptyStar} from 'assets/icons/star_border.svg';
import {Button, Icon} from 'app/UI/components';

import {CardProps} from './Card.types';
import styles from './Card.module.scss';


const Card: FC<CardProps> = props => {

  const renderStars = () => {
    const componentsToRender = [];

    for (let i = 1; i <= 5; i++) {
      if (i <= props.rating) {
        componentsToRender.push(<Star key={i} className={styles.FilledStar}/>);
      } else {
        componentsToRender.push(<EmptyStar key={i} className={styles.EmptyStar}/>);
      }
    }

    return (
      <div className={styles.StarsContainer}>
        {componentsToRender}
      </div>
    );
  };

  const renderImage = () => {
    if (props.image) {
      return (
        <img
          src={props.image}
          alt={`Product: ${props.image}`}
          className={classNames(styles.Image, {[styles.InactiveImage]: !props.active})}
        />);
    }

    return <Icon src='notFound' alt='Picture not found' className={styles.ImageNotFound}/>;
  };

  return (
    <div className={classNames(styles.Container, props.className)}>
      {renderImage()}
      {props.promo &&
      <div className={styles.PromoContainer}>
        <span>Promo</span>
      </div>
      }
      <div className={styles.TextContainer}>
        <h3>{props.name}</h3>
        <span>{props.description}</span>
      </div>
      {renderStars()}
      <Button
        onClick={props.setModalActive}
        className={styles.Button}
        isPrimary
        isDisabled={!props.active}
      >
        {props.active ? 'Show details' : 'Unavailable'}
      </Button>
    </div>
  );
};

export default Card;
