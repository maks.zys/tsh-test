import {ProductCardBasicProps} from '../../ProductCard.types';

type DetailsProps = ProductCardBasicProps & {
  onClose: () => void;
  isOpen?: boolean;
  className?: string;
};

export type {
  DetailsProps,
};
