import React, {FC} from 'react';
import classNames from 'classnames';

import {Icon} from 'app/UI/components/index';

import Modal from '../Modal/Modal';
import {DetailsProps} from './Details.types';
import styles from './Details.module.scss';


const Details: FC<DetailsProps> = props => {

  if (!props.isOpen) {
    return null;
  }

  return (
    <Modal onOutsideClick={props.onClose}>
      <div className={classNames(styles.Container, props.className)}>
        <Icon src='close' alt='Close icon' className={styles.Icon} onClick={props.onClose}/>
        <img src={props.image} alt={`Product: ${props.image}`}/>
        <div className={styles.TextContainer}>
          <h3>{props.name}</h3>
          <span>{props.description}</span>
        </div>
      </div>
    </Modal>
  );
};

export default Details;
