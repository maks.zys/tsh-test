import {MouseEvent} from 'react';

type ModalProps = {
  onOutsideClick: (event: MouseEvent) => void;
}

export type {
  ModalProps,
};
