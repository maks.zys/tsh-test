import React, {FC} from 'react';
import ReactDOM from 'react-dom';

import {ModalProps} from 'app/UI/components/ProductCard/components/Modal/Modal.types';
import styles from 'app/UI/components/ProductCard/components/Modal/Modal.module.scss';


const Component: FC<ModalProps> = props => (
  <div className={styles.ModalContainer} onClick={props.onOutsideClick}>
    <div onClick={(event => event.stopPropagation())}>
      {props.children}
    </div>
  </div>
);

const Modal: FC<ModalProps> = props => ReactDOM.createPortal(
  <Component {...props} />,
  document.getElementById('modal-root')!,
);

export default Modal;
