import Card from './Card/Card';
import Details from './Details/Details';
import Modal from './Modal/Modal';

export {
  Card,
  Details,
  Modal,
};
