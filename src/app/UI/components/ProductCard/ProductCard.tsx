import React, {FC, useState} from 'react';

import {Card, Details} from './components';

import {ProductCardProps} from './ProductCard.types';


const ProductCard: FC<ProductCardProps> = ({cardClassName, detailsClassName, ...props}) => {
  const [modalActive, setModalState] = useState(false);

  return (
    <>
      <Details {...props} isOpen={modalActive} onClose={() => setModalState(false)} className={detailsClassName}/>
      <Card {...props} setModalActive={() => setModalState(true)} className={cardClassName}/>
    </>
  );
};

export default ProductCard;
