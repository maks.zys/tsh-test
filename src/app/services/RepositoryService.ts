class RepositoryService {
  objectIntoURLSearchParams<T>(params: T) {
    const values: string[][] = [];
    Object.entries(params).forEach(([key, value]) => value && values.push([key, value.toString()]));

    return new URLSearchParams(values);
  }
}

export default new RepositoryService();
