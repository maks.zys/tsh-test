import ky, {Options} from 'ky';

import {API} from 'consts';


class HttpService {
  instance: typeof ky;

  constructor() {
    this.instance = ky.create({
      prefixUrl: API,
      timeout: 30000,
    } as Options);
  }

  setHeaders(headers: Pick<Options, 'headers'>) {
    this.instance = this.instance.extend(headers);
  }

  async request(path: (Request | URL | string), options: Omit<Options, 'prefixUrl'>): Promise<unknown> {
    return await this.instance(path, options).json();
  }
}

export default new HttpService();
